class Role < ActiveRecord::Base
    has_many :employees
    belongs_to :company

    serialize :permissions

    def full_permissions
        roles_description = File.read(Rails.root.join('lib', 'roles.json'))
        description = JSON.parse(roles_description)

        full_permissions = {}
        self.permissions ||= {}

        description.each do |cname, cdesc|
            full_permissions[cname] = {}
            full_permissions[cname]['alias'] = cdesc['alias']
            full_permissions[cname]['actions'] = {}
            cdesc['actions'].each do |aname, action_alias|
                full_permissions[cname]['actions'][aname] = {
                    alias: action_alias,
                    allowed: !!(
                        self.permissions[cname] && 
                        self.permissions[cname][aname] &&
                        self.permissions[cname][aname] == "true"
                    )
                }
            end
        end
        return full_permissions
    end
end
