class Report < ActiveRecord::Base
  belongs_to :room
  has_one :user, through: :room
  has_many :report_entries, dependent: :destroy
  accepts_nested_attributes_for :report_entries

  validates :room_id, presence: true
end
