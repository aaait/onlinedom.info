class User < ActiveRecord::Base
  attr_accessor :updating_password, :confirming, :profile_update
  attr_accessor :company_name

  belongs_to :meta, polymorphic: true
  belongs_to :role

  has_one :reset_key
  has_one :userpic, as: :owner, class_name: 'FileHandler'
  has_one :employee
  has_many :rooms
  has_many :reports, through: :rooms
  has_many :tickets

  serialize :phones
  enum user_type: ['admin', 'client', 'company']
  enum gender: ['male', 'female']

  validates :user_type, 
    presence: true,
    unless: [:updating_password, :confirming]
  validates :name, 
    presence: true,
    unless: [:updating_password, :confirming]
  validates :email, 
    presence: true, 
    format: /.+@.+\..+/, uniqueness:  true,
    unless: [:updating_password, :confirming]

  validates :password, 
    presence: true,
    length: { in: 6..20 },
    unless: :confirming

  after_initialize :init
  after_create :create_meta

  has_secure_password

  def userpic=(file)
    return FileHandler.create(
        file: file,
        is_public: true,
        description: 'userpic',
        owner: self
      )
  end

  def update_password(password, password_confirmation)
    @updating_password = true
    return self.update(password: password, 
      password_confirmation: password_confirmation)
  end

  def generate_key
    self.confirmation_key = rand(36**64).to_s(36)
  end

  def confirm!
    @confirming = true
    self.update confirmed: true
  end

  def company
    if self.user_type == 'company'
      return self.meta
    end
    if self.is_employee?
      return self.employee.company
    end
    return nil
  end

  def self.makepass
    rand(36**8).to_s(36)
  end

  def is_employee?
    self.employee.present?
  end

  def employer
    if self.is_employee?
      return self.employee.company
    else
      return nil
    end
  end

  private
  def init
    self.confirmed ||= false
  end

  def create_meta
    case self.user_type
    when 'company'
      self.update(meta: Company.create(company_name: self.company_name))
    when 'client'
      # todo: creaete Client model instance
    else end
  end
end
