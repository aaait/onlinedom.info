class Employee < ActiveRecord::Base
    belongs_to :user
    belongs_to :company
    belongs_to :role

    serialize :phones
end
