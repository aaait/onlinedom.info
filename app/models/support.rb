class Support < ActiveRecord::Base
  belongs_to :estate
  serialize :phones
end
