class Ticket < ActiveRecord::Base
  belongs_to :user
  belongs_to :executor, class_name: 'User'
  belongs_to :room

  before_create :set_defaults

  enum status: ['Создана', 'Просмотрена', 'В работе', 'Отклонено', 'Исполнено', 'Отменена']
  enum ticket_type: ['Обычная', 'Аварийная', 'VIP']

  validates :user, presence: true
  validates :room, presence: true

  serialize :meta

  def cancel
    self.status = 'Отменена'
    return self.save
  end

  def watch!(user)
    if self.user.id != user.id && self.status == 'Создана'
      self.executor = user
      self.status = 'Просмотрена'

      return self.save
    end
    return false
  end

  def owned_by(user)
    return true if self.user.id = user.id
    if self.room && self.room.estate && self.room.estate.company && user.company
      return self.room.estate.company.id = user.company.id
    end
    return false
  end

  def ticket_company
    if self.room && self.room.estate && self.room.estate.company && user.company
      return self.room.estate.company
    end
  end

  def editable(user)
    return false if ['Отменена', 'Исполнено', 'Отклонено'].include?(self.status)

    if user.user_type == 'company'
      return user.company == self.ticket_company
    else 
      return owned_by(user)
    end
  end

  private
  def set_defaults
    self.status ||= 'Создана'
    self.ticket_type ||= 'Обычная' 

    if self.room.estate && self.room.estate.company
      self.number = self.room.estate.company.tickets.count + 1
    else
      self.number = nil
    end
  end
end
