class FileHandler < ActiveRecord::Base
  attr_accessor :file
  before_validation :generate_name, unless: :name

  after_destroy :delete_file
  after_rollback :delete_file

  validates :name, uniqueness: true, presence: true
  validates :owner, presence: true

  belongs_to :owner, polymorphic: true

  def generate_name
    original_filename = @file.original_filename
    # generating file name
    loop do
      self.name = rand(36**64).to_s(36) + '.' + original_filename.split('.').last
      break unless FileHandler.find_by(name: self.name)
    end

    # replacing file

    File.open(self.filepath, 'wb') do |file|
      file.write(@file.read)
    end
  end

  def webpath
    if self.is_public
      '/uploads/' + self.name
    else
      nil
    end
  end

  def filepath
    if self.is_public
      Rails.root.join('public', 'uploads', self.name)
    else
      Rails.root.join('private', 'uploads', self.name)
    end
  end

  def delete_file
    if self.name
      File.delete(self.filepath) if File.exist?(self.filepath)
    end
  end
end
