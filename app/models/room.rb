class Room < ActiveRecord::Base
  before_create :link_estate

  belongs_to :user
  belongs_to :estate

  has_many :counters, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :tickets, dependent: :destroy

  accepts_nested_attributes_for :counters, allow_destroy: true

  serialize :meta
  serialize :map
  serialize :owner

  validates :address, presence: {
    message: 'Введите адрес квартиры'
  }
  validates :number, presence: {
    message: 'Введите номер квартиры'
  }

  enum room_type: [
    'Квартира', 
    'Нежилое помещение',
    'Дом',
    'Земельный участок',
    'Гараж',
    'Дача'
  ]

  def current_report
    self.reports
      .where(["created_at >  ?", Time.now - 1.month])
      .includes(:report_entries).last || false
  end

  def current_report_period
    last_report = self.current_report
    if last_report
      {start: last_report.created_at, end: (last_report.created_at + 1.month) }
    else
      now = Time.now
      return {
        start: Time.now - 1.month,
        end: Time.now
      }
    end
  end

  def set_owner(owner)
    if owner && owner['email']
      user = User.find_by(email: owner['email'])
      if user
        self.user_id = user.id
        user.update({
          'name' => owner['name'],
          'email' => owner['email'],
          'phones' => owner['phones'] || [''],
        })
        return self.owner = {registered: true}
      end
    end

    return set_unregistered_owner(owner)
  end

  private

  def set_unregistered_owner(owner)
    self.owner = {
      'name' => owner['name'],
      'email' => owner['email'],
      'phones' => owner['phones'] || [''],
      'registered' => false
    }
  end

  def link_estate
    estate = Estate.find_by(full_address: self.address)

    return true unless estate      
    if estate
      same_room = estate.rooms
        .where(
          'number = ? AND room_type = ?',
          self.number, Room.room_types[self.room_type]
        ).take

      if same_room && same_room.user_id
        self.errors[:user_id] = "Это помещение уже зарегестрировано другим пользователем"
        return false
      else
        self.estate_id = estate.id
        same_room.destroy if same_room
      end
    end
  end
end
  