class EmployeesController < ApplicationController
  before_action :check_user

  def show
    @employee = @user.company.employees.find_by(id: params[:id])

    unless @employee
      return render json: {
          error: "Сотрудник с id #{params[:id]} не найден"
          }, status: 404
    end

    return render 'show'
  end

  def list
    @employees = Employee.all.where(company_id: @user.company.id)
    return render 'list'
  end

  def create
    unless params[:email]
      return render json: {error: 'Email не был передан'}, status: 404
    end

    employeeUser = User.find_by(email: params[:email])
    if employeeUser && employeeUser.is_employee?
      return render json: {
        error: 'Пользователь уже является сотрудником'
      }, status: 422
    end

    unless employeeUser
      employeeUser = create_employee_user() 
      new_user = true
      if employeeUser.errors.any?
        return render json: {
          error: 'Ошибка создания пользователя',
          model_errors: employeeUser.errors
        }, status: 422
      end
    end

    employee = Employee.new(employee_params)
    employee.user = employeeUser
    employee.company = @user.company

    logger.debug(employee.user)

    if employee.save
      render json: { success: true }
      UserMailer.create_employee_user(employeeUser).deliver_now if new_user
      return
    else
      return render json: employee.errors
    end
  end

  def update
    @employee = @user.company.employees.find_by(id: params[:id])
    unless @employee
      return render json: {
          error: "Сотрудник с id #{params[:id]} не найден"
          }, status: 404
    end

    if @employee.update employee_params
      if params[:name] && params[:name].strip != ''
        unless @employee.user.update_attribute(:name, params[:name])
          return render json: {
            error: 'Не удалось обновить имя пользователя'
          }, status: 422
        end
      end
      return render json: {success: true}
    else
      return render json: @employee.errors, status: 422
    end
  end

  def delete
    @employee = @user.company.employees.find_by(id: params[:id])
    unless @employee
      return render json: {
        error: "Сотрудник с id #{params[:id]} не найден"
        }, status: 404
    end

    if @employee.destroy
      return render json: {success: true}
    else
      return render json: @employee.errors, status: 422
    end
  end

  private
  def create_employee_user()
    password = User.makepass
    return User.create(
        user_type: 'client',
        email: params[:email],
        password: password,
        password_confirmation: password,
        name: params[:name],
        phones: params[:phones],
        confirmed: true
      )
  end

  def check_user
    if @user.is_employee?
      return true
    end
    unless session[:user_type] == "company"
      return render json: {
        error: 'Пользователь не имеет доступа к компании'
        }, status: 403
    end
  end

  def employee_params
    params.permit(:role_id, phones: [:number, :extension])
  end
end