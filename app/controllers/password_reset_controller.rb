class PasswordResetController < ApplicationController
  layout false

  def create
    unless params[:email]
      return render json: {
        error: 'Не указан пользователь'
      }, status: 404
    end

    reset_user = User.find_by(email: params[:email])

    unless reset_user
      return render json: {
        error: 'Не найден пользователь'
      }, status: 404
    end

    reset_key = ResetKey.new(
        user: reset_user
      )
    reset_key.generate_key

    if reset_key.save
      render json: reset_key.id, status: 200
      UserMailer.password_reset_email(reset_user, reset_key.key).deliver_now
    else
      render json: reset_key.errors, status: 422
    end
  end

  def reset
    unless params[:key]
      @badkey = true
      return render 'reset', status: 404
    end

    @key = ResetKey.find_by(key: params[:key])

    unless @key
      @badkey = true
      return render 'reset', status: 404
    end

    if @key.outdated?
      @oldkey = true
      return render 'reset', status: 410
    end

    render 'reset'

    rescue ActiveRecord::RecordNotFound
      @badkey = true
      return render 'reset', status: 404
  end

  def update_password
    unless params[:key]
      return render json: {
        error: 'Ключ не найден'
      }, status: 404
    end

    @key = ResetKey.find_by(key: params[:key])
    unless @key
      return render json: {
        error: 'Ключ не найден'
      }, status: 404
    end

    if @key.outdated?
      return render json: {
        error: 'Ключ устарел'
      }, status: 410
    end

    if @key.user.update_password(
          params[:password],
          params[:password_confirmation]
        )
      @key.destroy
      return render json: @key.user.id, status: 200
    else
      return render json: @key.user.errors, status: 422
    end
  end
end
