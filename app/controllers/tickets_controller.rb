class TicketsController < ApplicationController
  # GET /api/tickets
  def index
    if session[:user_type] == 'company'
      @tickets = @user.company.tickets
    elsif session[:user_type] == 'client'
      @tickets = @user.tickets
    end
  end

  # GET /api/tickets/1
  def show
    set_ticket
    @ticket.watch!(@user) if @ticket
  end

  # POST /api/tickets
  def create
    return unless get_room
    @ticket = Ticket.new(ticket_params)

    set_executor

    @ticket.room = @room

    @ticket.user = @user
    if session[:user_type] == 'company'
      @ticket.executor_id ||= @user.id
    end


    if @ticket.save
      return render :show, status: :created
    else
      return render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/tickets/1
  def update
    set_ticket
    set_executor

    if params[:status]
      unless Ticket.statuses.keys.include?(params[:status])
        return render json: {errors: 'Некорректный статус'}, status: 422   
      end
    end

    if @ticket.update(update_ticket_params)
      return render :show, status: :ok, location: @ticket
    else
      return render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/tickets/1/status
  def update_status
    unless can_update?
      return render json: {
        error: 'Эту заявку нельзя редактировать'
      }, status: 403
    end

    unless params[:status]
      return render json: {errors: 'Не передан статус'}, status: 422   
    end
    unless Ticket.statuses.keys.include?(params[:status])
      return render json: {errors: 'Некорректный статус'}, status: 422   
    end

    respond_to do |format|
      if @ticket.save
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  def cancel_ticket
    unless can_update?
      render json: {
        error: 'Эту заявку нельзя редактировать'
      }, status: 403 unless performed?

      return false
    end

    if @ticket.update_attribute(:status, 'Отменена')
      render json: {success: true}, status: 200
    else
      render json: @ticket.errors, status: 422
    end
  end

  private
    def get_room
      room_found = get_room_by_id || get_room_by_number

      unless room_found
        render json: {
          error: 'Помещение не указано или не найдено'
        }, status: 422
      end

      return room_found
    end

    def get_room_by_number
      return false if !params[:room_number] && !params[:estate_id]
      return false unless session[:user_type] == 'company'

      @estate = @user.company.estates.find_by(id: params[:estate_id])
      return false unless @estate

      @room = @estate.rooms.find_by(number: params[:room_number]) || 
        Room.create(
          estate: @estate, 
          number: params[:room_number],
          address: @estate.address
        )

      return @room
    end

    def get_room_by_id
      return false unless params[:room_id]
      @room = @user.rooms.find_by(id: params[:room_id])

      if session[:user_type] == 'company' && !@room
        @room = @user.company.rooms.find_by(id: params[:room_id])
      end

      return @room
    end

    def can_update?
      set_ticket
      
      return false unless @ticket

      if session[:user_type] == 'client'
        return @ticket
      end

      if session[:user_type] == 'company'
        return @ticket.status == 'Создана'
      end
    end

    def set_ticket
      return @ticket if @ticket # caching

      # @ticket = @user.tickets.find_by(id: params[:id]) if params[:id]
      ticket = Ticket.find_by(id: params[:id])

      if ticket.owned_by(@user)
        @ticket = ticket
      else
        return render json: {errors: 'Заявка не найдена'}, status: 404
      end

      return @ticket
    end

    def set_executor

      return false unless @ticket
      return false unless params[:executor_id]
      return false unless session[:user_type] == 'company'

      @executor = User.find_by(id: params[:executor_id])
      return false unless @executor

      if @user.id == @executor.id || @user.company.id  == @executor.company.id
          return @ticket.executor = @executor
      end

      return false
    end

    def ticket_params
      params.permit(
        :ticket_type,
        meta: [:description, :applicant]
      )
    end

    def update_ticket_params
      params.permit(
        :status,
        :executor_id
      )
    end
end
