class RoomsController < ApplicationController
    def index
        @rooms = @user.rooms
    end

    def show
        get_room
    end

    def create
        @room = Room.new(rooms_params)
        @room.meta = params[:meta]
        @room.user = @user

        respond_to do |format|
            if @room.save
                format.json {
                    render json: @room, status: :created    
                }
            else
                format.json {
                    render json: @room.errors,
                        status: :unprocessable_entity
                }
            end
        end
    end

    def update
        get_room
        return unless @room

        @room.set_owner(params[:owner]) if params[:owner]

        respond_to do |format|
            if @room.update(rooms_params)
                format.json {
                    render json: @room, status: :ok    
                }
            else
                format.json {
                    render json: @room.errors,
                        status: :unprocessable_entity
                }
            end
        end
    end

    def destroy
        get_room
        return unless @room

        @room.meta = params[:meta]

        respond_to do |format|
            if @room.destroy
                format.json {
                    render json: {success: true}, status: :ok    
                }
            else
                format.json {
                    render json: @room.errors,
                        status: :unprocessable_entity
                }
            end
        end
    end

    private
    def get_room
        if @user.user_type == 'client'
            @room = @user.rooms.find_by(id: params[:id])
            unless @room
                render json: {error: 'Помещение не найдено'}, status: 404
            end
        end

        if @user.user_type  == 'company'
            
            room = Room.find_by(id: params[:id])
            print('----------LOG', room.estate.id, @user.company.id)

            if room && room.estate.company.id == @user.company.id
                @room = room
            else
                render json: {error: 'Помещение не найдено'}, status: 404
            end
        end
    end

    def rooms_params
    params.permit(:room_type,
        :estate_id,
        :address,
        :area,
        :tenants,
        :number,
        :company_name,
        :company_address,
        :company_phone,
        map: [:lat, :lng],
        counters_attributes: [
            :id,
            :_destroy,
            :place,
            :counter_type,
            :regnum,
            :name,
            :precision,
            :seal_date,
            :seal_number,
            :replacement_date
        ]
    )
    end
end