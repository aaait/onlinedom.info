class RolesController < ApplicationController
    before_action :check_user
    before_action :get_role, only: [:show, :update, :delete, :shows]

    def list
        render json: @user.company.roles
    end

    def create
        @role = Role.new(role_params)
        @role.company = @user.company

        if @role.save
            render json: {success: true}
        else
            render json: {
                error: "Ошибка создания роли",
                role_errors: @role.errors
            }, status: 422
        end
    end

    def update
        if @role.update(role_params)
            render json: {success: true}
        else
            render json: {
                error: "Ошибка обновления роли",
                role_errors: @role.errors
            }, status: 422
        end
    end

    def delete
        if @role.destroy
            render json: {success: true}
        else
            render json: {
                error: "Ошибка удаления роли",
                role_errors: @role.errors
            }, status: 422
        end
    end

    def show
        render json: @role.to_json(methods: :full_permissions)
    end

    def description
        roles = File.read(Rails.root.join('lib', 'roles.json'))
        render json: JSON.parse(roles)
    end

    private
    def role_params
        {
            name: params[:name],
            permissions: params[:permissions]
        }
    end

    def check_user
        if @user.is_employee?
            return true
        end

        unless session[:user_type] == "company"
          return render json: {
            error: 'Пользователь не имеет доступа к компании'
            }, status: 403
        end
    end

    def get_role
        unless params['id']
            return render json: {
                error: 'Необходим id'
            }, status: 404
        end

        @role = @user.company.roles.find_by(id: params['id'])

        unless @role
            return render json: {
                error: "Роль с id #{params[:id]} не найдена"
            }, status: 404
        end
    end
end