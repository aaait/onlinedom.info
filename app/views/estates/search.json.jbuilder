json.array!(@estates) do |estate|
  json.extract! estate, :id, :address, :estate_type, :license, :meta, :map, :full_address 
  json.supports_attributes estate.supports
  json.company estate.company

  json.has_files estate.has_license
end
