json.array!(@estates) do |estate|
  json.extract! estate,
      :id,
      :address,
      :estate_type,
      :meta,
      :license,
      :full_address,
      :rooms,
      :created_at, 
      :updated_at
end
