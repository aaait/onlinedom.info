json.(@user,
  :id,
  :user_type,
  :name,
  :email,
  :birthdate,
  :gender,
  :phones,
  :meta
)

if @user.userpic
  json.userpic @user.userpic.webpath
end


if session[:user_type] == 'company'
  if @user.meta && @user.meta.logo_file
    json.logo @user.meta.logo_file.webpath
  end
  json.has_innkpp_file @user.meta.present? && @user.meta.innkpp_file.present?
  json.has_ogrn_file @user.meta.present? && @user.meta.ogrn_file.present?
end

if session[:user_type] == 'company' || @user.is_employee?
  json.company @user.company
end

if @user.is_employee?
  json.is_employee true
  json.employee @user.employee
  json.employer @user.employer
  json.employee_role @user.employee.role
end