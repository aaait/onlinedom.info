json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :ticket_type, :status, :number, :meta, :created_at, :updated_at
  json.room ticket.room
    if session[:user_type] == 'company'
        json.executor ticket.executor
    end
    json.user ticket.user
    json.editable ticket.editable(@user)
end
