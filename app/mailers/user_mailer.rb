class UserMailer < ApplicationMailer
  def confirm_email(user)
    @user = user
    @key = @user.confirmation_key
    mail(
      to: @user.email,
      subject: 'Добро пожаловать на onlinedom.info'
    )
  end

  def password_reset_email(user, key)
    @key = key
    mail(
        to: user.email,
        subject: 'Восстановление пароля на onlinedom.info'
      )
  end

  def create_employee_user(user)
    @user = user
    mail(
        to: user.email,
        subject: 'Вы приглашены на сайт onlinedom.info в качестве сотрудника УК'
      )
  end
end
