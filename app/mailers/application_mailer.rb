class ApplicationMailer < ActionMailer::Base
  default from: "noreply@onlinedom.info"
  layout 'mailer'

  before_action { @domain = Rails.application.config.domain }
end
