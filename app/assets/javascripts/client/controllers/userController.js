angular.module('client').controller('clientController', ['$scope', 'userData', 'apiClient',
    function ($scope, userData, apiClient) {
        $scope.userData = userData.getData();
        $scope.token = window.__token;
        $scope.changeMode = function(){
            apiClient.mode.change($scope.token).then(function(data){
                location.reload();
            });
        }
    }]);
