angular.module('client').controller('profileController', ['$scope', 'apiService', 'alertService', '$timeout', '$filter', 'userData',
    function ($scope, apiService, alertService, $timeout, $filter, userData) {
        $scope.init = function() {
            $scope.profileModel = userData.getData();
            $scope.profileModel.authenticity_token = window.__token;
            if($scope.profileModel.phones == null){
                $scope.profileModel.phones = [];
            }
            $scope.spinnerState = false;
            $scope.submitSpinnerState = false;
        };

        $scope.addPhone = function(){
            $scope.profileModel.phones.push(
                {
                    number: ''
                }
            );
        };
        $scope.removePhone = function(index){
            $scope.profileModel.phones.splice(index, 1);
        };

        $scope.fileReaderSupported = window.FileReader != null;
        $scope.uploadPhoto = function(files){
            $scope.spinnerState = true;
            if (files != null) {
                var file = files[0];
                if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function(){
                                apiService.uploadUserPhoto(file, $scope.profileModel.authenticity_token).then(function(){
                                    $scope.spinnerState = false;
                                    $scope.profileModel.userpic = e.target.result;

                                }, function(){
                                    $scope.spinnerState = false;
                                    alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                                });
                            });
                        }
                    });
                }
            }
        };

        $scope.removeLogo = function(elementId){
            $('#' + elementId).val('');
            $scope.spinnerState = true;
            apiService.deleteUserPhoto($scope.profileModel.authenticity_token).then(function(){
                $scope.spinnerState = false;
                $scope.profileModel.userpic = '';
            }, function(){
                $scope.spinnerState = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            });
        };

        $scope.submitForm = function(){
            $scope.submitSpinnerState = true;
            $scope.profileModel.birthdate = $filter('date')($scope.profileModel.birthdate, 'dd-MM-yyyy');
            apiService.updateUserInfo($scope.profileModel).then(function(){
                $scope.submitSpinnerState = false;
                alertService.updateAlert('success', 'Обновлено успешно');
            }, function(){
                $scope.submitSpinnerState = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            });
        }
    }]);
