angular.module('client').directive('isActiveNav', [ '$location', function($location) {
    var link = function(scope, element, attrs) {
        scope.$watch(function() { return $location.path(); },
            function(path) {
                path = path.substring(1);
                var url = element.attr('href');
                if (url) {
                    url = url.substring(1);
                }
                if (path == url) {
                    element.addClass("active");
                } else {
                    element.removeClass('active');
                }
            });
    };
    return {
        restrict: 'A',
        link: link
    };
}]);