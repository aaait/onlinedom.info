// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require vendor
//= require_self
//= require_tree ./utils

angular.module('login', ['ngRoute', 'apiService', 'alertService'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'template/login-index',
                controller: 'loginCtrl'
            })
            .when('/register', {
                templateUrl: 'template/login-register',
                controller: 'registerController'
            }).when('/restore', {
                templateUrl: 'template/login-password-restore',
                controller: 'restoreController'
            }).when('/reset/:key', {
                templateUrl: function(params){ return '/reset/' + params.key; },
                controller: 'resetController'
            })
            .otherwise({
                redirectTo:'/'
            });
    }])
    .controller('loginCtrl', ['$scope', 'apiService', 'alertService',
    function ($scope, apiService, alertService) {
        $scope.userInfo = {
            'email' : '',
            'password': '',
            'authenticity_token' : window.__token
        };
        $scope.buttonDisabled = false;
        $scope.sendLoginData = function(){
            $scope.buttonDisabled = true;
            if(!$scope.notMatchingPass && !$scope.notMatchingPassConf) {
                apiService.login($scope.userInfo)
                    .then(function(response){
                        if(response.status == '200'){
                            window.location.href = '/';
                            $scope.buttonDisabled = false;
                        }
                    }, function(response){
                        alertService.hideAlert();
                        alertService.setText(response.data.error);
                        alertService.setClass('alert-danger');
                        alertService.showAlert();
                        $scope.buttonDisabled = false;
                    })
            }
        }

    }])
    .controller('registerController', ['$scope', '$location', 'apiService', 'alertService', function ($scope, $location, apiService, alertService) {
        $scope.userInfo = {
            'name' : '',
            'company_name': '',
            'email' : '',
            'password': '',
            'password_conf': '',
            'user_type': 'client',
            'authenticity_token' : window.__token
        };
        $scope.buttonDisabled = false;
        $scope.notMatchingPassConf = false;
        $scope.notMatchingPass = false;
        $scope.checkMatch = function () {
            if($scope.userInfo.password.length < 6){
                $scope.notMatchingPass = true;
            }else{
                $scope.notMatchingPass = false;
            }

            if( !$scope.userInfo.password_conf ||
                !$scope.userInfo.password_conf.trim()) return;
            if($scope.userInfo.password != $scope.userInfo.password_conf || $scope.userInfo.password_conf.length < 6) {
                $scope.notMatchingPassConf = true;
            }else{
                $scope.notMatchingPassConf = false;
            }
        };

        $scope.sendRegistrationData = function(){
            $scope.buttonDisabled = true;
            if(!$scope.notMatchingPass && !$scope.notMatchingPassConf) {
                apiService.registerNewUser($scope.userInfo)
                    .then(function(){
                        alertService.hideAlert();
                        alertService.setText('Вы успешно зарегистрированны, проверьте свою почту для подтверждения регистрации');
                        alertService.setClass('alert-success');
                        alertService.showAlert();
                        $location.path('/');
                        $scope.buttonDisabled = false;
                    }, function(response){
                        alertService.hideAlert();
                        if(response.data.email[0] == 'has already been taken'){
                            alertService.setText('Такой email уже зарегестрирован');
                        }else {
                            alertService.setText('Что-то пошло не так, попробуйте еше раз');
                        }
                        alertService.setClass('alert-danger');
                        alertService.showAlert();
                        $scope.buttonDisabled = false;
                    })
            }else{
                $scope.buttonDisabled = false;
            }
        };
    }])
    .controller('restoreController', ['$scope', '$location', 'apiService', 'alertService', function ($scope, $location, apiService, alertService) {
        $scope.userInfo = {
            'email' : '',
            'authenticity_token' : window.__token
        };
        $scope.buttonDisabled = false;
        $scope.sendRestoreData = function(){
            $scope.buttonDisabled = true;
            apiService.restorePassword($scope.userInfo)
                .then(function(){
                    alertService.hideAlert();
                    alertService.setText('Для восстановления вашего аккаунта перейдите по ссылке, отправленной на вашу почту');
                    alertService.setClass('alert-success');
                    alertService.showAlert();
                    $location.path('/');
                    $scope.buttonDisabled = false;
                }, function(response){
                    alertService.hideAlert();
                    if(response.statusText == 'Not Found'){
                        alertService.setText('Пользователь с введенным email не найден');
                    }else{
                        alertService.setText('Что-то пошло не так, попробуйте еше раз');
                    }
                    alertService.setClass('alert-danger');
                    alertService.showAlert();
                    $scope.buttonDisabled = false;
                })
            };
    }])
    .controller('resetController', ['$scope', '$location', 'apiService', 'alertService', function ($scope, $location, apiService, alertService) {
        $scope.userInfo = {
            'password' : '',
            'password_conf' : '',
            'authenticity_token' : window.__token,
            'key' : ''
        };
        $scope.buttonDisabled = false;
        $scope.notMatchingPassConf = false;
        $scope.notMatchingPass = false;
        $scope.checkMatch = function () {
            if($scope.userInfo.password.length < 6){
                $scope.notMatchingPass = true;
            }else{
                $scope.notMatchingPass = false;
            }

            if( !$scope.userInfo.password_conf ||
                !$scope.userInfo.password_conf.trim()) return;
            if($scope.userInfo.password != $scope.userInfo.password_conf || $scope.userInfo.password_conf.length < 6) {
                $scope.notMatchingPassConf = true;
            }else{
                $scope.notMatchingPassConf = false;
            }
        };

        $scope.sendResetPasswordData = function(){
            $scope.buttonDisabled = true;
            if(!$scope.notMatchingPass && !$scope.notMatchingPassConf) {
                apiService.resetPassword($scope.userInfo)
                    .then(function(){
                        alertService.hideAlert();
                        alertService.setText('Пароль успешно изменен');
                        alertService.setClass('alert-success');
                        alertService.showAlert();
                        $location.path('/');
                        $scope.buttonDisabled = false;
                    }, function(response){
                        alertService.hideAlert();
                        if(response.status == 410){
                            alertService.setText('Ключ для смены пароля устарел');
                        }else{
                            alertService.setText('Ключ для смены не найден');
                        }
                        alertService.setClass('alert-danger');
                        alertService.showAlert();
                        $scope.buttonDisabled = false;
                    })
            }else{
                alertService.hideAlert();
                alertService.setText('Пароли должны совпадать');
                alertService.setClass('alert-danger');
                alertService.showAlert();
                $scope.buttonDisabled = false;
            }
        };
    }])
    .directive('alert',['alertService', function (alertService) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'template/login-alert',
            link: {
                pre: function(scope) {
                    scope.alert = alertService;
                }
            }
        }
    }])
;
