// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require vendor
//= require_self
//= require_tree ./company
//= require_tree ./utils
var app = angular.module('company', ['ngRoute', 'ngMessages', 'ui.mask', 'ngMap', 'apiCompany', 'alertService', 'smart-table']), userInfo;
app.config(['$routeProvider', function($routeProvider, $routeParams) {
        $routeProvider
            .when('/', {
                redirectTo: function(){
                    if(userInfo.employee_role){
                        var permissions = userInfo.employee_role.permissions;
                        var sections = {
                            companies: 'information',
                            employees: 'employees',
                            roles: 'roles',
                            rooms: 'estates',
                            requests: 'requests',
                            counters: 'counters'
                        }
                        for (sectionKey in sections) {
                            if (permissions[sectionKey]) {
                                return sections[sectionKey];
                            }
                        }
                        return 'error/403';
                    }else{
                        return 'information';
                    }
                }
            })
            .when('/information', {
                templateUrl: 'template/company/company-information',
                controller: 'informationController'
            })
            .when('/employees', {
                templateUrl: 'template/company/company-employees',
                controller: 'employeesController',
                resolve: {
                    employeesList: ['resolveApiService', function(resolveApi){
                        return resolveApi.getEmployeesList();
                    }]
                }
            })
            .when('/addEmployee', {
                templateUrl: 'template/company/company-employees-addition',
                controller: 'addEmployeeController',
                resolve: {
                    rolesList: ['resolveApiService', function(resolveApi) {
                        return resolveApi.getRolesList();
                    }]
                }
            })
            .when('/editEmployee/:empId', {
                templateUrl: 'template/company/company-employees-edit',
                controller: 'editEmployeeController',
                resolve: {
                    rolesList: ['resolveApiService', function(resolveApi) {
                        return resolveApi.getRolesList();
                    }],
                    empData: ['resolveApiService', '$route', function(resolveApi, $route) {
                        return resolveApi.getEmployeeInfo($route.current.params.empId);
                    }]
                }
            })
            .when('/roles', {
                templateUrl: 'template/company/company-roles',
                controller: 'rolesController',
                resolve: {
                    rolesList: ['resolveApiService', function(resolveApi) {
                        return resolveApi.getRolesList();
                    }]
                }
            })
            .when('/addRole', {
                templateUrl: 'template/company/company-roles-addition',
                controller: 'addRoleController',
                resolve: { rolesDesc: ['resolveApiService', function(resolveApi) {
                        return resolveApi.getRolesDesc();
                    }]
                }
            })
            .when('/editRole/:roleId', {
                templateUrl: 'template/company/company-roles-edit',
                controller: 'editRoleController',
                resolve: {
                    roleData:
                    ['resolveApiService', '$route', function(resolveApi, $route) {
                        return resolveApi.getRole($route.current.params.roleId);
                    }]
                }
            })
            .when('/estates', {
                templateUrl: 'template/company/company-estates',
                controller: 'estatesController',
                resolve: {
                    objectsList: ['resolveApiService', function(resolveApi) {
                        return resolveApi.getEstatesList();
                    }]
                }
            })
            .when('/addEstate', {
                templateUrl: 'template/company/company-estates-addition-steps',
                controller: 'estateStepsController',
                resolve: {
                    objectData: function(){
                      return undefined;
                    }
                }
            })
            .when('/editEstate/:estateId', {
                templateUrl: 'template/company/company-estates-addition-steps',
                controller: 'estateStepsController',
                resolve: {
                    objectData:
                        ['resolveApiService', '$route', function(resolveApi, $route) {
                        return resolveApi.getEstateInfo($route.current.params.estateId);
                    }],
                    // supportData:
                    //     ['resolveApiService', '$route', function(resolveApi, $route) {
                    //         return resolveApi.getEstateSupportsInfo($route.current.params.estateId);
                    // }]
                }
            })
            .when('/requests', {
                templateUrl: 'template/company/company-requests',
                controller: 'requestsController',
                resolve: {
                    list: ['resolveApiService', function(resolveApi){
                        return resolveApi.getRequestsList();
                    }]
                }
            })
            .when('/addRequest', {
                templateUrl: 'template/company/company-requests-addition',
                controller: 'addRequestController',
                resolve: {
                    estates: ['resolveApiService', function(resolveApi){
                        return resolveApi.getEstatesList();
                    }],
                    employees: ['resolveApiService', function(resolveApi){
                        return resolveApi.getEmployeesList();
                    }],
                }
            })
            .when('/showRequest/:reqId', {
                templateUrl: 'template/company/company-request-show',
                controller: 'showRequestController',
                resolve: {
                    request: ['resolveApiService', '$route', function(resolveApi, $route){
                        return resolveApi.getRequest($route.current.params.reqId);
                    }]
                }
            })
            .when('/editRequest/:reqId', {
                templateUrl: 'template/company/company-request-edit',
                controller: 'editRequestController',
                resolve: {
                    request: ['resolveApiService', '$route', function(resolveApi, $route){
                        return resolveApi.getRequest($route.current.params.reqId);
                    }],
                    employees: ['resolveApiService', function(resolveApi){
                        return resolveApi.getEmployeesList();
                    }],
                }
            })
            .when('/counters', {
                templateUrl: 'template/company/company-counters',
                controller: 'counterAddressesController',
                resolve: {
                    objectsList: ['resolveApiService', function(resolveApi) {
                        return resolveApi.getEstatesList();
                    }]
                }
            })
            .when('/counters/estate/:estateId', {
                templateUrl: 'template/company/company-counters-rooms',
                controller: 'counterRoomsController',
                resolve: {
                    objectsList:
                        ['resolveApiService', '$route', function(resolveApi, $route) {
                        return resolveApi.getEstateInfo($route.current.params.estateId);
                    }],
                }
            })
            .when('/error/:errorId', {
                templateUrl: function(params) {
                        return 'template/error-' + params.errorId;
                    }
            })
            .otherwise({
                redirectTo:'/error/404'
            });
    }]);
    // TODO обрабатывать ошибки с resolve
app.run(["$rootScope", "$location", "userData", "routePermissions", function($rootScope, $location, userData, routePermissions) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if(next.$$route) {
            var routeToCheck = next.$$route.originalPath;
            if (routeToCheck.indexOf('error') == -1 && routeToCheck.length > 1) {
                if (!routePermissions.checkRoutePermissions(routeToCheck)) {
                    $location.path("/error/403");
                }
            }
        }
    });
    userData.setData(userInfo);
}]);
angular.element(document).ready(function() {
    $.get('api/users/me/', function(data) {
        userInfo = data;
        angular.bootstrap(document, ['company']);
    })
});
