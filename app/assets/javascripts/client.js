// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require vendor
//= require_self
//= require_tree ./client
//= require_tree ./utils
var app = angular.module('client', ['materialDatePicker', 'ngMessages', 'ngRoute', 'ui.mask', 'ngMap', 'ngSanitize', 'ui.select', 'apiService', 'apiClient', 'alertService', 'smart-table', 'userDataService']), userInfo;
app.config(['$routeProvider', function($routeProvider, $routeParams) {
        $routeProvider
            .when('/', {
                redirectTo:'/profile'
            })
            .when('/profile', {
                templateUrl: 'template/user/user-profile',
                controller: 'profileController'
            })
            .when('/rooms', {
                templateUrl: 'template/user/user-rooms',
                controller: 'roomsController',
                resolve: {
                    list: ['resolveClientApiService', function(resolveApi){
                        return resolveApi.getRoomsList();
                    }]
                }
            })
            .when('/addRoom', {
                templateUrl: 'template/user/user-rooms-addition',
                controller: 'addRoomController',
            })
            .when('/editRoom/:roomId', {
                templateUrl: 'template/user/user-rooms-addition',
                controller: 'editRoomController',
                resolve: {
                    room: ['resolveClientApiService', '$route', function(resolveApi, $route){
                        return resolveApi.getRoom($route.current.params.roomId);
                    }]
                }
            })
            .when('/counters', {
                templateUrl: 'template/user/user-counters',
                controller: 'countersController',
                resolve: {
                    list: ['resolveClientApiService', function(resolveApi){
                        return resolveApi.getRoomsList();
                    }]
                }
            })
            .when('/counters/:roomId', {
                templateUrl: 'template/user/user-counters-data',
                controller: 'сountersDataController',
                resolve: {
                    room: ['resolveClientApiService', '$route', function(resolveApi, $route){
                        return resolveApi.getRoom($route.current.params.roomId);
                    }]
                }
            })
            .when('/requests', {
                templateUrl: 'template/user/user-requests',
                controller: 'requestsController',
                resolve: {
                    list: ['resolveClientApiService', function(resolveApi){
                        return resolveApi.getRequestsList();
                    }]
                }
            })
            .when('/addRequest', {
                templateUrl: 'template/user/user-requests-addition',
                controller: 'addRequestController',
                resolve: {
                    rooms: ['resolveClientApiService', function(resolveApi){
                        return resolveApi.getRoomsList();
                    }]
                }
            })
            .when('/editRequest/:reqId', {
                templateUrl: 'template/user/user-requests-edit',
                controller: 'editRequestController',
                resolve: {
                    request: ['resolveClientApiService', '$route', function(resolveApi, $route){
                        return resolveApi.getRequest($route.current.params.reqId);
                    }]
                }
            })
            .otherwise({
                redirectTo:'/profile'
            });
    }])
;
// TODO обрабатывать ошибки с resolve
app.run(["userData", function(userData) {
    userData.setData(userInfo);
}]);
angular.element(document).ready(function() {
    $.get('api/users/me/', function(data) {
        userInfo = data;
        angular.bootstrap(document, ['client']);
    })
});
