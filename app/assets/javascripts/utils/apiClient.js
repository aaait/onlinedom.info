angular
    .module('apiClient', [])
    .factory('apiClient', ['$http', function($http) {
        //var urlBase = '/api/customers';
        var service = {
            room: {
                add: function(params){
                    return $http({
                        url: 'api/rooms/',
                        method: "POST",
                        data: params
                    });
                },
                edit: function(params){
                    return $http({
                        url: 'api/rooms/' + params.id,
                        method: "PUT",
                        data: params
                    });
                },
                delete: function(id, token){
                    return $http.delete('/api/rooms/' + id,
                    {
                        params: {
                            'authenticity_token': token
                        }
                    });
                }
            },
            report: {
                send: function(params){
                    return $http({
                        url: 'api/reports/',
                        method: "POST",
                        data: params
                    });
                }
            },

            request: {
                add: function(params){
                    return $http({
                        url: 'api/tickets/',
                        method: "POST",
                        data: params
                    });
                },
                edit: function(params){
                    return $http({
                        url: 'api/tickets/' + params.id,
                        method: "PUT",
                        data: params
                    });
                },
                delete: function(id, token){
                    return $http.delete('/api/tickets/' + id,
                    {
                        params: {
                            'authenticity_token': token
                        }
                    });
                }
            },
            mode: {
                change: function(token){
                    return $http({
                        url: '/users/mode/company',
                        method: "PUT",
                        params: {
                            'authenticity_token': token
                        }
                    });
                }
            },
            estates:{
                find: function(query){
                    return $http({
                        url: 'api/estates/search/' + query,
                        method: "GET",
                    });
                }
            }

        }
        return service;
    }]);
