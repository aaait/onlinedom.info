angular.module('userDataService', []).factory('userData', ['$rootScope', function ($rootScope) {
    var userData, userPermissions;
    return {
        setData: function(data) {
            userData = data;
            if(data.employee_role){
                userPermissions = data.employee_role.permissions;
            }
        },
        getData: function(){
            return userData;
        },
        userCan: function(controller, action){
            if(userData.user_type != 'company') {
                return userPermissions[controller][action];
            }else{
                return true;
            }
        }
    };
}]);
