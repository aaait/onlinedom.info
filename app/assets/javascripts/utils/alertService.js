angular
    .module('alertService', [])
    .factory('alertService', function() {
        var alert = {
            'class': '',
            'text': '',
            'show' : false
        };

        alert.setClass = function (classname) {
            alert.class = classname;
        };

        alert.setText = function (text) {
            alert.text = text;
        };

        alert.showAlert = function(){
            alert.show = true;
        };

        alert.hideAlert = function(){
            alert.show = false;
        };

        alert.updateAlert = function(type, text){
            alert.hideAlert();
            alert.setText(text);
            alert.setClass('alert-' + type);
            alert.showAlert();
        };

        return alert;
    });