angular.module('company').directive('estatesSteps', [ 'apiCompany', function(apiCompany) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
          estate: "="
        },
        templateUrl: 'template/directives/estate-steps',
        link: function(scope, element, attrs, controllers) {
          scope.estateModel = scope.estate || {
            address: {
                region: '',
                city: '',
                street: '',
                number: '',
                building: '',
                housing:''
            },
            meta: {
                housing_number: '',
                nonresident_number: ''
            },
            license: {
                series: '',
                number: '',
                issue_date: '',
                issued_by: ''
            }
          };
          scope.estateModel.authenticity_token = window.__token;
          scope.roomsList = {};
          scope.buttonDisabled = scope.estateModel.id ? false : true;
          scope.steps = [
              '1. Информация об объекте',
              '2. Добавление пользователей',
              '3. Добавление присоединенных сетей',
              '4. Настройка формы квитанции',
            ];
          scope.selection = 0;

          scope.getCurrentStepIndex = function(){
          return scope.selection;
          };
          scope.goToStep = function(index) {
          if (typeof scope.steps[index] !== 'undefined')
          {
            scope.selection = index;
          }
          };

          scope.hasNextStep = function(){
          var stepIndex = scope.getCurrentStepIndex();
          var nextStep = stepIndex + 1;
          return (typeof scope.steps[nextStep] !== 'undefined');
          };

          scope.hasPreviousStep = function(){
          var stepIndex = scope.getCurrentStepIndex();
          var previousStep = stepIndex - 1;
          return (typeof scope.steps[previousStep] !== 'undefined');
          };

          function incrementStep() {
          if ( scope.hasNextStep() )
          {
            var stepIndex = scope.getCurrentStepIndex();
            var nextStep = stepIndex + 1;
            scope.selection = nextStep;
          }
          };
          scope.onButtonClick = function(){
            if(scope.getCurrentStepIndex() == 0){
              console.log(scope.estateModel);
                createEstate(scope.estateModel);
            }else{
              incrementStep();
            }
          }
          scope.decrementStep = function() {
          if ( scope.hasPreviousStep() )
          {
            var stepIndex = scope.getCurrentStepIndex();
            var previousStep = stepIndex - 1;
            scope.selection = previousStep;
          }
          };

          function createEstate(model){
            apiCompany.addOrUpdateEstate(model).then(function(result){
              scope.roomsList = result.data.rooms;
              incrementStep();
            }, function(response){
                if(response.status == 422){
                    alertService.updateAlert('danger', response.data.full_address[0]);
                }else{
                    alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                }
            })
          }
          }
          }
}]);
