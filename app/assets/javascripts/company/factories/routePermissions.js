angular.module('company').factory('routePermissions', ['$rootScope', 'userData', function ($rootScope, userData) {
    var cfg = {
        '/information': {
            controller: 'companies',
            action: 'update'
        },
        '/employees': {
            controller: 'employees',
            action: 'list'
        },
        '/addEmployee': {
            controller: 'employees',
            action: 'create'
        },
        '/editEmployee/:empId': {
            controller: 'employees',
            action: 'update'
        },
        '/roles': {
            controller: 'roles',
            action: 'list'
        },
        '/addRole': {
            controller: 'roles',
            action: 'create'
        },
        '/editRole/:roleId': {
            controller: 'roles',
            action: 'update'
        },
        '/estates': {
            controller: 'rooms',
            action: 'list'
        },
        '/addEstate': {
            controller: 'rooms',
            action: 'create'
        },
        '/editEstate/:estateId': {
            controller: 'rooms',
            action: 'update'
        },
        '/requests': {
            controller: 'requests',
            action: 'list'
        },
        '/addRequest': {
            controller: 'requests',
            action: 'create'
        },
        '/editRequest/:reqId': {
            controller: 'requests',
            action: 'update'
        },
        '/showRequest/:reqId': {
            controller: 'requests',
            action: 'list'
        },
        '/counters': {
            controller: 'counters',
            action: 'list'
        },
        '/counters/estate/:estateId': {
            controller: 'counters',
            action: 'update'
        }
    };
    return {
        checkRoutePermissions: function(route) {
            if(userData.getData().user_type != 'company') {
                console.log(route);
                console.log('cfg[route]', cfg[route]);
                if(cfg[route]){
                    var controller = cfg[route].controller;
                    var action = cfg[route].action;
                    return userData.userCan(controller, action);
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }
    };
}]);
