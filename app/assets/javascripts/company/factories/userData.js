angular.module('company').factory('userData', ['$rootScope', function ($rootScope) {
    var userData, userPermissions;
    return {
        setData: function(data) {
            userData = data;
            if(data.employee_role){
                userPermissions = data.employee_role.permissions;
            }
        },
        getData: function(){
            return userData;
        },
        userCan: function(controller, action){
            if(userData.user_type != 'company') {
                if(userPermissions[controller]){
                    return userPermissions[controller][action];
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }
    };
}]);
