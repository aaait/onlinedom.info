angular.module('company')
    .factory('resolveApiService', ['$http', function($http) {
        var service = {
            getRolesList: function(){
                return $http({
                    url: 'api/roles/list/',
                    method: "GET"
                })
            },

            getRolesDesc: function() {
                return $http({
                    url: 'api/roles/description/',
                    method: "GET"
                })
            },

            getRole: function(id) {
                return $http({
                    url: 'api/roles/' + id,
                    method: "GET"
                })
            },

            getEmployeesList: function(){
                return $http({
                    url: 'api/employees/list.json',
                    method: "GET"
                })
            },

            getEmployeeInfo: function(empId){
                return $http({
                    url: 'api/employees/' + empId + '.json',
                    method: "GET"
                })
            },

            getEstatesList: function(){
                return $http({
                    url: 'api/estates/',
                    method: "GET"
                })
            },

            getEstateInfo: function(estId){
                return $http({
                    url: 'api/estates/' + estId + '.json',
                    method: "GET"
                })
            },

            getEstateSupportsInfo: function(estId){
                return $http({
                    url: 'api/estates/' + estId + '/supports',
                    method: "GET"
                })
            },

            getRequestsList: function(){
                return $http({
                    url: 'api/tickets/',
                    method: "GET"
                })
            },

            getRequest: function(id){
                return $http({
                    url: 'api/tickets/' + id,
                    method: "GET"
                })
            }
        };

        return service;
    }]);
