angular.module('company').controller('requestsController', ['$scope', 'apiCompany', 'alertService', 'list',
    function ($scope, apiCompany, alertService, list) {
        $scope.init = function() {
            $scope.requests = list.data;
            if($scope.requests.length > 0){
                $scope.requestsSafe = angular.copy($scope.requests);
            }
            $scope.authenticity_token = window.__token;
        };

        $scope.setObjIdToRemove = function(id, index){
            $scope.selectedObjId = id;
            $scope.selectedObjIndex = index;
        };
        $scope.deleteSelectedObj = function(id, index){
            $scope.buttonDisabled = true;
            apiCompany.request.delete($scope.selectedObjId, $scope.authenticity_token).then(function(){
                $scope.buttonDisabled = false;
                $('#remove-dialog').modal('hide');
                $scope.requests[index].status = "Отменена";
                $scope.requests[index].editable = false;
                alertService.updateAlert('success', 'Заявка отменена');
            }, function(){
                $scope.buttonDisabled = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
    }]);
