angular.module('company').controller('addRequestController', ['$scope', '$location', 'apiCompany', 'alertService', 'estates', 'employees',
    function ($scope, $location, apiCompany, alertService, estates, employees) {
        $scope.init = function() {
            $scope.estates = estates.data;
            $scope.employees = employees.data;
            $scope.requestModel = {
                estate_id: $scope.estates[0].id,
                executor_id: $scope.employees[0].user.id,
                ticket_type: 'Обычная',
                room_number: '',
                meta: {
                    description: '',
                    applicant: ''
                },
                authenticity_token: window.__token
            }
            $scope.showSpinner = false;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.request.add($scope.requestModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Заявка успешно заведена');
                $location.path('/requests');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }
    }]);
