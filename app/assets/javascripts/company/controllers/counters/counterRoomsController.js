angular.module('company').controller('counterRoomsController', ['$scope', 'apiCompany', 'alertService', '$location', 'objectsList',
    function ($scope, apiCompany, alertService, $location, objectsList) {
        $scope.init = function() {
            $scope.estatesModel = objectsList.data;
            if($scope.estatesModel.rooms.length > 0){
                $scope.estatesSafe = angular.copy($scope.estatesModel.rooms);
            }
        };
    }]);
