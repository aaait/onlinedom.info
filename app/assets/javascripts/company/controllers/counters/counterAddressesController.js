angular.module('company').controller('counterAddressesController', ['$scope', 'apiCompany', 'alertService', '$location', 'objectsList',
    function ($scope, apiCompany, alertService, $location, objectsList) {
        $scope.init = function() {
            $scope.estatesModel = {
                estatesList: objectsList.data,
                authenticity_token: window.__token
            };
            if($scope.estatesModel.estatesList.length > 0){
                $scope.estatesSafe = angular.copy($scope.estatesModel.estatesList);
            }
        };
    }]);
