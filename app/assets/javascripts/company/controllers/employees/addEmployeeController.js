angular.module('company').controller('addEmployeeController', ['$scope', 'apiCompany', 'alertService', '$location', 'rolesList',
    function ($scope, apiCompany, alertService, $location, rolesList) {
        $scope.init = function() {
            $scope.rolesList = rolesList.data;
            $scope.empModel = {
                name: '',
                email: '',
                phones: [{
                    number: '',
                    extension: ''
                }],
                roleName: 'Выберите',
                role_id: '',
                authenticity_token: window.__token
            };
            $scope.showSpinner = false;
        };
        $scope.addPhone = function(){
            $scope.empModel.phones.push(
                {
                    number: ''
                }
            );
        };
        $scope.removePhone = function(index){
            $scope.empModel.phones.splice(index, 1);
        };
        $scope.selectRole = function(roleName, roleId){
            $scope.empModel.roleName = roleName;
            $scope.empModel.role_id = roleId;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.addEmployee($scope.empModel).then(function(){
                alertService.updateAlert('success', 'Сотрудник добавлен успешно');
                $scope.showSpinner = false;
                $location.path('/employees');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        }

    }]);