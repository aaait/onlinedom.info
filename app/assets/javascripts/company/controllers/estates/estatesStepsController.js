angular.module('company').controller('estateStepsController', ['$scope', 'apiCompany', 'alertService', '$location', 'objectData',
    function ($scope, apiCompany, alertService, $location, objectData) {
      $scope.estate = objectData ? objectData.data : undefined;
    }]);
