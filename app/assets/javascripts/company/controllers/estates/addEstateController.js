angular.module('company').controller('addEstateController', ['$scope', 'apiCompany', 'alertService', '$location', '$timeout',
    function ($scope, apiCompany, alertService, $location, $timeout) {
        $scope.init = function(){
            $scope.showSpinner = false;
            $scope.estateModel = {
                address: {
                    region: '',
                    city: '',
                    street: '',
                    number: '',
                    building: '',
                    housing:''
                },
                map: {
                    lat: '',
                    lng: ''
                },
                meta: {
                    living: '',
                    nonliving: ''
                },
                supports_attributes: [],
                has_files: false,
                authenticity_token : window.__token
            };
            $scope.fileModel = {
                file: ''
            };
            $scope.initSupportModel();
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.addEstate($scope.estateModel).then(function(result){
                return result.data.id;
            }, function(response){
                if(response.status == 422){
                    alertService.updateAlert('danger', response.data.full_address[0]);
                }else{
                    alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                }
                $scope.showSpinner = false;
            }).then(function(estate_id){
                if($scope.fileModel.file != ''){
                    apiCompany.uploadLicense($scope.fileModel.file, $scope.estateModel.authenticity_token, estate_id).then(function(){
                        alertService.updateAlert('success', 'Обьект успешно добавлен');
                        $scope.showSpinner = false;
                        $location.path('/estates');
                    }, function(){
                        alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                        $scope.showSpinner = false;
                    })
                }else{
                    $location.path('/estates');
                }
            });
        };

        $scope.initSupportModel = function(){
            $scope.selectedSupportModel = {
                name: '',
                address: '',
                phones: [
                    {
                        number: '',
                        extension: ''
                    }
                ]
            };
            $scope.selectedSupportIndex = '';
            $scope.isEditMode = false;
        };

        $scope.addSupport = function(){
            $scope.estateModel.supports_attributes.push($scope.selectedSupportModel);
            $scope.initSupportModel();
            $('#add-support-dialog').modal('hide');
        };
        $scope.updateSupport = function(){
            $scope.estateModel.supports_attributes[$scope.selectedSupportIndex] = $scope.selectedSupportModel;
            $scope.initSupportModel();
            $('#add-support-dialog').modal('hide');
        };
        $scope.setSelectedSupportIndex = function(index){
            $scope.selectedSupportIndex = index;
        };
        $scope.deleteSelectedSupport = function(index){
            $scope.estateModel.supports_attributes[index]._destroy = true;
            $('#remove-dialog').modal('hide');
        };
        $scope.setSelectedSupportData = function(index){
            $scope.isEditMode = true;
            $scope.setSelectedSupportIndex(index);
            $scope.selectedSupportModel = $scope.estateModel.supports_attributes[index];
        };

        $scope.fileReaderSupported = window.FileReader != null;
        $scope.uploadLicense = function(files){
            if (files != null) {
                var file = files[0];
                if ($scope.fileReaderSupported) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function(){
                                $scope.fileModel.file = file;
                                $scope.estateModel.has_files = true;
                            });
                        }
                    });
                }
            }
        };
        $scope.removeLicense = function(){
            $scope.fileModel.file = '';
            $scope.estateModel.has_files = false;
        }

    }]);
