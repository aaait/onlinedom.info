angular.module('company').controller('estatesController', ['$scope', 'apiCompany', 'alertService', '$location', 'objectsList',
    function ($scope, apiCompany, alertService, $location, objectsList) {
        $scope.init = function() {
            $scope.estatesModel = {
                estatesList: [],
                estatesAmount: 0,
                authenticity_token: window.__token
            };
            $scope.estatesModel.estatesList = $scope.estatesModel.estatesList.concat(objectsList.data);
            $scope.estatesModel.estatesAmount = objectsList.data.length;
            if($scope.estatesModel.estatesAmount > 0){
                $scope.estatesSafe = angular.copy($scope.estatesModel.estatesList);
            }
        };
        $scope.setObjIdToRemove = function(id, index){
            $scope.selectedObjId = id;
            $scope.selectedObjIndex = index;
        };
        $scope.deleteSelectedObj = function(id, index){
            $scope.buttonDisabled = true;
            apiCompany.deleteEstate(id, $scope.estatesModel.authenticity_token).then(function(){
                $scope.buttonDisabled = false;
                $('#remove-dialog').modal('hide');
                $scope.estatesModel.estatesList.splice(index, 1);
                alertService.updateAlert('success', 'Объект удален');
            }, function(){
                $scope.buttonDisabled = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
    }]);
