class CreateAddressLinks < ActiveRecord::Migration
  def change
    create_table :address_links do |t|
      t.references :estate, index: true, foreign_key: true
      t.string :address, index: true

      t.timestamps null: false
    end
  end
end
