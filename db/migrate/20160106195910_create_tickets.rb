class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.references :user, index: true, foreign_key: true
      t.references :executor, index: true
      t.references :room, index: true, foreign_key: true
      t.text :meta
      t.integer :ticket_type
      t.integer :status

      t.timestamps null: false
    end
  end
end
