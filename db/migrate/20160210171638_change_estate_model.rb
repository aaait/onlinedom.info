class ChangeEstateModel < ActiveRecord::Migration
  def change
    rename_column :estates, :address, :full_address
    add_column :estates, :address, :text
  end
end
