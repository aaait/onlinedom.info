class CreateSupports < ActiveRecord::Migration
  def change
    create_table :supports do |t|
      t.string :name
      t.text :phones
      t.string :address
      t.references :estate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
