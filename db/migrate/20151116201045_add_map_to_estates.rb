class AddMapToEstates < ActiveRecord::Migration
  def change
    add_column :estates, :map, :text
  end
end
