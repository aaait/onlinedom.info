class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.integer :place
      t.integer :counter_type
      t.string :regnum
      t.string :name
      t.string :precision
      t.date :seal_date
      t.string :seal_number
      t.date :replacement_date

      t.references :room

      t.timestamps null: false
    end
  end
end
