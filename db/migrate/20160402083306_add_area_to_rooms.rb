class AddAreaToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :area, :decimal, precision: 10, scale: 2
  end
end
