class AddOwnerToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :owner, :text
  end
end
