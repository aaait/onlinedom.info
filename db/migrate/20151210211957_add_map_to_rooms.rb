class AddMapToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :map, :text
  end
end
