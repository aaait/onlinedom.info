class AddNumberToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :number, :bigint
  end
end
