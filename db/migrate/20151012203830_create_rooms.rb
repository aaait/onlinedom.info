class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :user, index: true, foreign_key: true
      t.references :estate

      t.integer :room_type
      t.string :address, index: true

      t.string :company_name
      t.string :company_address
      t.string :company_phone

      t.text :description

      t.timestamps null: false
    end
  end
end
