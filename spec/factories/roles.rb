FactoryGirl.define do
  factory :role do
    name "test role"
    permissions({ "employees" => {
          "show" => "true",
          "list" => "true"
        }
      })
  end
end
