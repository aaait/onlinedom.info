FactoryGirl.define do
  factory :room do
    # user create(:confirmed_user)
    room_type "Квартира"
    address "д. Простоквашино"
    company_name "Test Company"
    company_address "Test address"
    company_phone "333 55 777"
    area 45.55
    tenants 5
    number 45

    transient do
      counters_count 5
    end

    after(:create) do |room, evaluator|
      create_list(:counter, evaluator.counters_count, room: room)
    end
  end
end
