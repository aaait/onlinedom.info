FactoryGirl.define do
  factory :estate do
    address({
      'region' => 'lorem',
      'city' => 'ipsum',
      'street' => 'dolor',
      'number' => '21'
    })
    license({
      'series' => '666',
      'number' => '543272',
      'issue_date' => '21.02.2012',
      'issued_by' => 'Дядя Вася'
    })
    meta({
      'housing_number' => 10,
      'nonresident_number' => 10
    })
  end
end
