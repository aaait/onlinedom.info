FactoryGirl.define do
  factory :user do
    name 'Vasili Pupkin'
    email { rand(36**10).to_s(36) + '@factory.net' }
    password 'qwerty123'
    password_confirmation 'qwerty123'
    user_type 'client'
    factory :confirmed_user do |variable|
      confirmed true

      factory :user_client do
        user_type 'client'
      end

      factory :user_company do
        user_type 'company'
      end
    end
  end
end