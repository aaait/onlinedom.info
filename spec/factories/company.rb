FactoryGirl.define do
  factory :company do
    association :user, factory: :user_company
    contact_person 'Vasili Pupkin'
    phones [
      {'number' => '79202134343'},
      {'number' => '79514359933'}
    ]
    country 'Россия'
    city 'Рязань'
    address 'ул. Ленина д.1'
    post_code '390001'
    inn "7632339181"
    kpp "032349"
    bank_account "83078253304572987708" 
    bik "246571044"
    coordinates 'lat' => '54.621893', 'lng' => '39.755302'
    info 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.
     Amet ipsa laudantium magni sapiente dolore deleniti accusamu
     s dolorum inventore quis aut odio, corrupti sed adipisci 
     ut ratione est sit enim fugit!'
  end
end