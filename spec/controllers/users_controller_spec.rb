require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'login' do
    it 'should redirect to login, if is not authorized' do
      get :index
      expect(response).to redirect_to(:login)
    end

    it 'should redirect to index if logged in' do
      post :auth, {
        email: 'admin@onlinedom.info', password: 'jm50tyn2'
      }
      get :login
      expect(response).to redirect_to('/')
    end

    it 'should login user with correct user and password' do
      post :auth, {
        email: 'admin@onlinedom.info', password: 'jm50tyn2'
      }
      get :index
      expect(response).not_to redirect_to(:login)
    end

    it 'should not redirect to login, if is authorized' do
      post :auth, {
        email: 'admin@onlinedom.info', password: 'jm50tyn2'
      }
      get :index
      expect(response).not_to redirect_to(:login)
    end

    it 'should fails login with unexisting email' do
      post :auth, {
        email: 'vasya@pupkin.ru', password: 'jm50tyn2'
      }
      expected = {
        error: 'Нет пользователя с такой почтой/паролем'
      }.to_json
      expect(response.body).to eq(expected)
    end

    it 'should fails login with incorrect password' do
      post :auth, {
        email: 'admin@onlinedom.info', password: '123'
      }
      expected = {
        error: 'Нет пользователя с такой почтой/паролем'
      }.to_json
      expect(response.body).to eq(expected)
    end
  end

  describe 'confirmation' do
    it 'should not login unconfirmed user' do
      post :create, {
        name: 'Vasili Pupkin',
        email: 'vpupkin1@mail.ru',
        password: 'qwerty123',
        password_confirmation: 'qwerty123',
        user_type: 'client'
      }
      post :auth, {
        email: 'vpupkin1@mail.ru', password: 'qwerty123'
      }
      expect(response).to have_http_status(403)
    end

    it 'should confirm user' do
      post :create, {
        name: 'Vasili Pupkin',
        email: 'vpupkin2@mail.ru',
        password: 'qwerty123',
        password_confirmation: 'qwerty123',
        user_type: 'client'
      }
      user = User.last
      get :confirm, {key: user.confirmation_key}
      expect(flash[:success]).to be_present
    end

    it 'should not confirm user with invalid key' do
      post :create, {
        name: 'Vasili Pupkin',
        email: 'vpupkin3@mail.ru',
        password: 'qwerty123',
        password_confirmation: 'qwerty123',
        user_type: 'client'
      }
      get :confirm, {key: 'notvalidgkey'}
      expect(User.last.confirmed).to be false
    end
    
    it 'should redirect confirmed user to\
        root when trying to confirm'
  end

  describe 'logout' do
    it 'should logout user' do
      post :auth, {
        email: 'admin@onlinedom.info', password: 'jm50tyn2'
      }
      get :logout
      get :index
      expect(response).to redirect_to(:login)
    end
  end

  describe 'register' do
    it 'should register new client' do
      expect{
          post :create, {
            name: 'Vasili Pupkin',
            email: 'vpupkin@mail.ru',
            password: 'qwerty123',
            password_confirmation: 'qwerty123',
            user_type: 'client'
          }
        }.to change{User.count}.by(1)
    end

    it 'should register new company' do
      expect{
          post :create, {
            name: 'OOO "Roga&Copyta"',
            email: 'rk.inc@mail.ru',
            password: 'qwerty123',
            password_confirmation: 'qwerty123',
            user_type: 'company'
          }
        }.to change{User.count}.by(1)
    end

    it 'should not register as admin' do
      expect{
          post :create, {
            name: 'h@cker',
            email: 'hkrhkr@mail.ru',
            password: 'qwerty123',
            password_confirmation: 'qwerty123',
            user_type: 'admin'
          }
        }.to change{User.count}.by(0)
    end

    it 'should not register with incorrect data' do
      expect{
          post :create, {
            email: 'vpupkin@mail.ru',
            password: 'qwerty123',
            password_confirmation: 'qwerty123',
            user_type: 'client'
          }
        }.to change{User.count}.by(0)
    end

    it 'should not register without user_type' do
      expect{
          post :create, {
            name: 'OOO "Roga&Copyta"',
            email: 'rk.inc@mail.ru',
            password: 'qwerty123',
            password_confirmation: 'qwerty123',
          }
        }.to change{User.count}.by(0)
    end

    it 'should not register with existing email' do
      expect{
          post :create, {
            name: 'OOO "Roga&Copyta"',
            email: 'djently@yandex.ru',
            password: 'qwerty123',
            password_confirmation: 'qwerty123',
            user_type: 'company'
          }
        }.to change{User.count}.by(0)
    end
  end

  describe 'meta' do
    it 'should return empty response if not logged in' do
      get :me, format: :json
      expect(response.body).to be_empty
    end

    it 'should return 404 if not logged in' do
      get :me, format: :json
      expect(response).to have_http_status(404)
    end

    it 'should return 200 if user logged in' do
      post :auth, {
        email: 'admin@onlinedom.info', password: 'jm50tyn2', 
      }
      get :me, format: :json
      expect(response).to have_http_status(200)
    end
  end

  describe 'profile' do
    before(:each) do
      @user = create(:user)
      session[:user] = @user.id
    end

    it 'should update profile information' do
      profile = {
        name: 'Stas Kovalev',
        birthdate: '24-06-1992',
        gender: 'male',
        phones: ['4353545t4', '4354353']
      }
      post :update, profile

      expect(response).to have_http_status(200)
    end
    it 'should upload userpic' do 
      userpic = fixture_file_upload('files/testlogo.png', 'image/png')
      expect{
        post :upload_userpic, {file: userpic}
        }.to change{
          FileHandler.count
        }.by(1)
    end
    it 'should delete userpic' do
      userpic = fixture_file_upload('files/testlogo.png', 'image/png')
      post :upload_userpic, {file: userpic}
      expect{
        delete :delete_userpic
        }.to change{
          FileHandler.count
        }.by(-1)
    end

    it 'should change password' do
      post :change_password, {
        current_password: attributes_for(:user)['password'],
        password: 'qwerty123',
        password_confirmation: 'qwerty123'
      }

      expect(response).to have_http_status(200)
    end
  end
end
