require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  before(:each) do
    user = create(:user)
    session[:user] = user.id
  end

  it 'should create room' do
    post :create, attributes_for(:room)
    expect(response).to have_http_status(200)
  end

  it 'should update room' do
    post :create, attributes_for(:room)
    
    req = attributes_for(:room)
    req['id'] = Room.last.id
    put :update, req

    expect(response).to have_http_status(200)    
  end

  it 'should return rooms list' do
    3.times { post :create, attributes_for(:room) }
    get :list

    expect(response.body).not_to be_empty
  end

  it 'should delete room' do
    3.times { post :create, attributes_for(:room) }
    id = Room.last.id

    expect{
      delete :delete, {id: id}
    }.to change{Room.count}.by(-1)
  end
end