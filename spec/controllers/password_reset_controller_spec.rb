require 'rails_helper'

RSpec.describe PasswordResetController, type: :controller do
  describe 'reset password' do
    it 'should create new reset key' do
      expect{
          post :create, {
            email: User.last.email
          }
        }.to change{ResetKey.count}.by(1)
    end

    it 'should response with error if user doesn\'t exists' do
      post :create, {
        email: 'no@such.mail'
      }
      expected = {
        error: 'Не найден пользователь'
      }.to_json
      expect(response.body).to eq(expected)
    end

    it 'should not reset password with key older than a day' do
      key = ResetKey.new(user_id: User.last.id)
      key.generate_key
      key.created_at = Time.now - 24*60*60 - 60
      key.save

      get :reset, {key: key.key}
      expect(response).to have_http_status(410)
    end

    it 'should reset password with correct key' do
      user = User.last
      key = ResetKey.new(user_id: user.id)
      key.generate_key
      key.save

      post :update_password, {
        key: key.key,
        password: 'newpassword',
        password_confirmation: 'newpassword'
      }

      expect(response).to have_http_status(200)
    end

    it 'should not reset password with incorrect key' do
      user = User.last
      key = ResetKey.new(user_id: user.id)
      key.generate_key
      key.save

      post :update_password, {
        key: 'adsadsads',
        password: 'newpassword',
        password_confirmation: 'newpassword'
      }

      expect(response).not_to have_http_status(200)
    end

    it 'should delete activated key' do
      user = User.last
      key = ResetKey.new(user_id: user.id)
      key.generate_key
      key.save

      expect{
          post :update_password, {
            key: key.key,
            password: 'newpassword',
            password_confirmation: 'newpassword'
          }
        }.to change{ResetKey.count}.by(-1)
    end

    it 'should send mail with reset key'
  end
end
