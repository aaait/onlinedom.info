require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
    before(:each) do
        # login as company
        @user = User.find_by(email: 'user@domgon.ru')
        session[:user] = @user.id
    end

    it 'should upate unverified company' do
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(response.body).to eq( {success: true}.to_json )
    end

    it 'should create update request for verified company' do
        @user.meta.verify!
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(response.body).to eq({
          success: 'Создан запрос на обновление данных'
          }.to_json )
    end

    it 'can\'t self-update non-company meta' do
      user = User.first
      session[:user] = user.id

      company_attributes = attributes_for(:company)
      post :update, company_attributes

      expect(response.body).to eq({
        error: 'Пользователь не прикреплен к компании'
        }.to_json)
    end


    it 'should save phone numbers' do
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(@user.meta.phones).to eq(company_attributes[:phones])
    end

    it 'should save coordinates' do
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(@user.meta.coordinates).to eq(company_attributes[:coordinates])
    end

    describe 'files' do
      before(:each) do
        @user = User.find_by(email: 'user@domgon.ru')
        session[:user] = @user.id
        @company = create(:company)
        @user.meta = @company
      end

      it 'should create logo file handler' do
        logo_file = fixture_file_upload('files/testlogo.png', 'image/png')
        
        expect{
          post :upload_file, {file: logo_file, type: 'logo'}
        }.to change{
          FileHandler.count
        }.by(1)
      end

      it 'should replace logo file' do
        logo_file = fixture_file_upload('files/testlogo.png', 'image/png')
        post :upload_file, {file: logo_file, type: 'logo'}
        
        expect{
          post :upload_file, {file: logo_file, type: 'logo'}
        }.not_to change{
          FileHandler.count
        }
      end
      it 'should create innkpp file'
      it 'should replace innkpp file'
      it 'should create ogrn file'
      it 'should replace ogrn file'

      it 'should delete logo_file' do
        logo_file = fixture_file_upload('files/testlogo.png', 'image/png')
        post :upload_file, {file: logo_file, type: 'logo'}

        expect{
          delete :delete_file, type: 'logo'
          }.to change{
            FileHandler.count
          }.by(-1)
      end
    end

    describe 'permissions' do
      it 'can be possible for admin to update company'
      it 'shouldn\'t update other companie\'s model'
    end
end
