# require 'rails_helper'

# RSpec.describe "Supports", type: :request do
#     before(:each) do
#         @user = User.find_by(email: "user@domgon.ru")
#         @company = @user.company
#         post '/login', email: "user@domgon.ru", password: 'jm50tyn2'
#         post '/api/estates', format: :json,
#             address: {
#                 region: 'Рязанская Область',
#                 city: 'Рязань',
#                 street: 'Братиславская',
#                 number: '21'
#             }

#         @estate = Estate.last
#     end

#     describe "GET /api/estates/:estate_id/supports" do
#         it "should return estates list" do
#             get "/api/estates/#{@estate.id}/supports"
#             expect(response).to have_http_status(200)

#             result = JSON.parse(response.body)
#             expect(result.count).to be(2)
#         end
#     end

#     describe "POST /api/estates/:estate_id/supports" do
#       it "should create new estates's support" do
#         expect{
#           post "/api/estates/#{@estate.id}/supports",
#             name: 'test estate',
#             address: 'test estate address',
#             phones: [{number: '24354'}, {number: '43523543', extension: '022'}]
#         }.to change{@estate.supports.count}.by(1)
#       end
#     end

#     describe "GET /api/estates/:estate_id/supports/:id" do
#       it 'should return support model by id' do
#         post "/api/estates/#{@estate.id}/supports",
#           name: 'test estate',
#           address: 'test estate address',
#           phones: [{number: '24354'}, {number: '43523543', extension: '022'}]
#         get "/api/estates/#{@estate.id}/supports/#{Support.last.id}"
#         expect(response).to have_http_status(200)
#       end
#     end

#     describe "PATCH /api/estates/:estate_id/supports/:id" do
#       it 'should update support model' do
#         post "/api/estates/#{@estate.id}/supports",
#           name: 'test estate',
#           address: 'test estate address',
#           phones: [{number: '24354'}, {number: '43523543', extension: '022'}]
#         patch "/api/estates/#{@estate.id}/supports/#{Support.last.id}", name: "wowow"
#         get "/api/estates/#{@estate.id}/supports/#{Support.last.id}"
#         result = JSON.parse(response.body)
#         expect(result['name']).to eq('wowow')
#       end
#     end

#     describe "DELETE /api/estates/:estate_id/supports/:id" do
#       it "should delete Support model" do
#         post "/api/estates/#{@estate.id}/supports",
#           name: 'test estate',
#           address: 'test estate address',
#           phones: [{number: '24354'}, {number: '43523543', extension: '022'}]

#         id = JSON.parse(response.body)['id']
#         delete "/api/estates/#{@estate.id}/supports/#{id}"
#         expect(response).to have_http_status(200)

#         get "/api/estates/#{@estate.id}/supports/#{id}"
#         expect(response).to have_http_status(404)
#       end
#     end
# end
