require 'rails_helper'
require 'json'

RSpec.describe "Roles API", type: :request do
  before(:each) do
    @user = create(:user_company)
    @company = @user.company

    post '/login', {email: @user.email, password: @user.password}
  end

  it 'should return roles list' do
    # get /api/roles/list
    3.times { create(:role, company: @company) }

    expect(response).to have_http_status(200)

    get '/api/roles/list'
    roles_list = ActiveSupport::JSON.decode(response.body)
    expect(roles_list.count).to eq(3)
  end

  it 'should create new role' do
    # post /api/roles
    # get /api/roles/:id
    role_params = {
      "name" => 'test role',
      "permissions" => {
        "employees" => {
          "show" => "true",
          "list" => "true"
        }
      }
    }
    post '/api/roles', role_params

    expect(response).to have_http_status(200)

    get "/api/roles/#{Role.last.id}"
    expect(response).to have_http_status(200)

    got_role = ActiveSupport::JSON.decode(response.body)
    expect(got_role.slice('name', 'permissions')).to eq(role_params)
  end

  it 'should update role' do
    role = create(:role, company: @company)
    role.name = "newname"
    role.permissions = {
      "companies" => {"update" => "true"},
      "requests" => {"list" => "true"}
    }

    put "/api/roles/#{role.id}", role.slice(:name, :permissions)
    expect(response).to have_http_status(200)

    get "/api/roles/#{role.id}"
    expect(response).to have_http_status(200)

    updated_role = ActiveSupport::JSON.decode(response.body)
      .slice('id', 'name', 'permissions')
      
    role = ActiveSupport::JSON.decode(
        ActiveSupport::JSON.encode(role)
      ).slice('id', 'name', 'permissions')

    expect(updated_role).to eq(role)
  end

  it 'should show role' do
    role = create(:role, company: @company)

    get "/api/roles/#{role.id}"
    expect(response).to have_http_status(200)
    response_json = ActiveSupport::JSON.decode(response.body)
    expect({
      id: response_json['id'],
      name: response_json['name'],
      permissions: response_json['permissions']
    }).to eq({
      id: role.id,
      name: role.name,
      permissions: role.permissions
    })
  end

  it 'should delete role' do
    role = create(:role, company: @company)

    expect{
        delete "/api/roles/#{role.id}"
      }.to change{
        Role.count
      }.by(-1)
  end

  it 'should send json with roles description' do
    # get /api/roles/description
    # testing if route works
    get '/api/roles/description'
    expect(response).to have_http_status(200)

    # testing if valid json sent
    response_json = ActiveSupport::JSON.decode(response.body)
    description_file = File.read(Rails.root.join('lib', 'roles.json'))
    file_json = ActiveSupport::JSON.decode(description_file)
    expect(response_json).to eq(file_json)
  end


  it 'should return employee role permissions via api/users/me' do
    employee = create(:employee)
    post '/login', {
      email: employee.user.email, password: employee.user.password
    }
    expect(response).to have_http_status(200)

    get '/api/users/me'
    expect(response).to have_http_status(200)
    permissions = ActiveSupport::JSON.decode(response.body)
    expect(permissions['employee_role']['permissions']).to eq(employee.role.permissions)
  end

  it 'should be possible to assign role to employee' do
    employee = create(:employee, company: @company)
    role = create(:role, company: @company)

    expect(employee.role_id).to_not eq(role.id)

    put "/api/employees/#{employee.id}", {
      phones: employee.phones,
      role_id: role.id
    }
    expect(response).to have_http_status(200)

    get "/api/employees/#{employee.id}.json"
    expect(response).to have_http_status(200)
    response_json = ActiveSupport::JSON.decode(response.body)
    expect(response_json["role"]["id"]).to eq(role.id)
  end
end