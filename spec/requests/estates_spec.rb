require 'rails_helper'

RSpec.describe "Estates", type: :request do
    before(:each) do
        @user = User.find_by(email: "user@domgon.ru")
        @company = @user.company
        post '/login', email: "user@domgon.ru", password: 'jm50tyn2'
    end


    describe "GET /api/estates" do
        it "should load estates list" do
            get '/api/estates'
            expect(response).to have_http_status(200)
        end
    end

    describe "GET /api/estates/:estate_id" do
        it "should load estates list" do
            @estate = create(:estate, company: @company)
            get "/api/estates/#{@estate.id}"
            expect(response).to have_http_status(200)
        end

        it "should not return others company estate" do
            other_estate = create(:estate, company_id: @company.id + 1)

            get "/api/estates/#{other_estate.id}"
            expect(response).to have_http_status(404)
        end
    end

    describe "POST /api/estates" do
        it 'should create new estate with rooms' do
            estate_attrs = attributes_for(:estate)
            rooms_number = estate_attrs[:meta]['housing_number'] + 
                estate_attrs[:meta]['nonresident_number']

            expect{
                post '/api/estates', estate_attrs
            }.to change{Room.count}.by rooms_number

            expect(response).to have_http_status(201)

            response_estate = JSON.parse(response.body)
            expect(response_estate['rooms'].length).to eq(rooms_number)
        end

        it 'should link existed rooms to estate, if any' do
            # runned creaete to build address
            estate = create(:estate).destroy
            rooms_number = estate.meta['housing_number'] + 
                estate.meta['nonresident_number']

            rooms = Room.create([
                { address: estate.full_address, number: 1, room_type: 'Квартира'},
                { address: estate.full_address, number: 2, room_type: 'Квартира'},
                { address: estate.full_address, number: 3, room_type: 'Квартира'}
            ])

            post '/api/estates', attributes_for(:estate)
            expect(response).to have_http_status(201)
            response_estate = JSON.parse(response.body)
            response_estate_id = response_estate['id'].to_i;

            rooms.each do |room|
                expect(Room.find(room.id).estate_id).to eq(response_estate_id)
            end

            expect(response_estate['rooms'].length).to eq(rooms_number)
        end

        it 'cant be possible to create 2 estates with the same address' do
            create(:estate)
            post '/api/estates', attributes_for(:estate)

            expect(response).to have_http_status(422)
        end
    end
end
