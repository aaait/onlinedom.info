require 'rails_helper'

RSpec.describe "Rooms", type: :request do
    before(:each) do
        @user = User.find_by(email: "djently@yandex.ru")
        post '/login', email: "djently@yandex.ru", password: 'jm50tyn2' 

        @room = Room.create(
            user: @user,
            number: 45,
            address: 'test address',
            room_type: 'Квартира',
            company_name: 'test company',
            company_address: 'test company_address',
            company_phone: '343564',
            meta: {test: 'test'},
            counters_attributes: [{
                    place: 'Кухня',
                    counter_type: 'Отопление',
                    regnum: '12344525',
                    name: 'test counter',
                    precision: '0.5',
                    seal_date: '24-05-2015',
                    seal_number: '24525244',
                    replacement_date: '25-06-2018'
                }]
        )
    end

    describe "GET /api/rooms" do
        it "should load rooms list" do
            get '/api/rooms'
            expect(response).to have_http_status(200)
        end 
    end

    describe "GET /api/rooms/:id" do
        it 'should render room info if user owns a room' do
            get "/api/rooms/#{@room.id}"
            expect(response).to have_http_status(200)
        end
    end

    describe "POST /api/rooms" do
        it 'should create new room with nested counter' do
            expect{
                post '/api/rooms',
                    address: 'test address',
                    room_type: 'Квартира',
                    number: 44,
                    company_name: 'test company',
                    company_address: 'test company_address',
                    company_phone: '343564',
                    meta: {test: 'test'},
                    counters_attributes: [{
                            place: 'Кухня',
                            counter_type: 'Отопление',
                            regnum: '12344525',
                            name: 'test counter',
                            precision: '0.5',
                            seal_date: '24-05-2015',
                            seal_number: '24525244',
                            replacement_date: '25-06-2018'
                        }]
            }.to change{@user.rooms.count}.by(1)

            expect(@user.rooms.last.counters).to be_present
        end

        it 'should bind room to existing estate' do
            @estate = create(:estate)
            post '/api/rooms', attributes_for(
                :room, address: @estate.full_address, number: 1
            )

            response_room = JSON.parse(response.body)
            expect(response_room['estate_id']).to eq(@estate.id)
        end

        it 'should replace room for existing estate' do
            @estate = create(:estate)

            expect{
                post '/api/rooms', attributes_for(
                    :room, address: @estate.full_address, number: 1
            )}.not_to change{ @estate.rooms.count }
        end

        it 'cant be possible to change rooms owner' do
            @estate = create(:estate)
            @estate.rooms.first.update(user_id: @user.id + 1)

            post '/api/rooms', attributes_for(
                :room, address: @estate.full_address, number: 1
            )

            expect(response).to have_http_status(422)
        end
    end

    describe "PUT " do
        it 'should update model' do
            patch "/api/rooms/#{@room.id}",
                address: 'test address',
                room_type: 'Квартира',
                number: 43,
                counters_attributes: [{
                        place: 'Кухня',
                        counter_type: 'Отопление',
                        replacement_date: '25-06-2018'
                    }]
            expect(response).to have_http_status(200)
            expect(@user.rooms.last.counters.count).to be(2)
        end

        it 'should remove counter on update' do
            expect{
                patch "/api/rooms/#{@room.id}",
                    counters_attributes: [{
                            _destroy: true,
                            id: @room.counters.last.id
                        }]
            }.to change{@room.counters.count}.by(-1)
        end

        it 'should set owner data if owner not registered' do
            patch "/api/rooms/#{@room.id}",
                owner: {
                    'name' => 'testname',
                    'email' => '452352346565363@mail.ru'
                }
            expect(response).to have_http_status(200)

            response_room = JSON.parse(response.body)
            expect(response_room['owner']['registered']).to be(false)
        end

        it 'should bind room to existing user when pass owner' do
            patch "/api/rooms/#{@room.id}",
                owner: {
                    'name' => 'testname',
                    'email' => @user.email
                }
            expect(response).to have_http_status(200)

            response_room = JSON.parse(response.body)
            expect(response_room['owner']['registered']).to be(true)
        end
    end

    describe "DELETE /api/rooms/id" do
        it 'should destroy room' do
            expect{
                delete "/api/rooms/#{@room.id}"
            }.to change{@user.rooms.count}.by(-1)
            expect(response).to have_http_status(200)
        end

        it 'should destroy nested room counters' do
            expect{
                delete "/api/rooms/#{@room.id}"
            }.to change{Counter.count}.by(-1)
            expect(response).to have_http_status(200)
        end
    end
end
