require 'rails_helper'

RSpec.describe "Tickets", type: :request do
    before(:each) do
        @user = create(:user_client)
        post '/login', email: @user.email, password: @user.password

        @room = create(:room, user: @user, counters_count: 3)
    end

    describe "GET /api/tickets" do
        it "should get list of tickets" do
            get '/api/tickets'
            expect(response).to have_http_status(200)
        end

        it "should get list of tickets for company" do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            get '/api/tickets'
            expect(response).to have_http_status(200)
        end
    end

    describe 'GET /api/tickets/:id' do
        before(:each) do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)
        end
        it 'should respond with ticket model' do
            get "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(200)
        end

        it 'should change ticket status when executor watches ticket' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            get "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(200)
        end
    end

    describe 'POST /api/tickets' do
        it 'should be possible to create ticket for client' do
            post '/api/tickets', attributes_for(:ticket, room_id: @room.id)
            expect(response).to have_http_status(201)
        end

        xit 'should be possible to create ticket for company' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            post '/api/tickets', attributes_for(:ticket, room_id: @room.id)
            expect(response).to have_http_status(201)
        end

        it 'should create ticket with statuses' do
            post '/api/tickets', attributes_for(
                :ticket, room_id: @room.id)
            expect(response).to have_http_status(201)
            expect(Ticket.last.ticket_type).to eq('Обычная')

            post '/api/tickets', attributes_for(
                :ticket, room_id: @room.id, ticket_type: 'Аварийная')
            expect(response).to have_http_status(201)
            expect(Ticket.last.ticket_type).to eq('Аварийная')

            post '/api/tickets', attributes_for(
                :ticket, room_id: @room.id, ticket_type: 'VIP')
            expect(response).to have_http_status(201)
            expect(Ticket.last.ticket_type).to eq('VIP')
        end
    end

    describe 'POST /api/tickets/:id' do
        it 'should update ticket'
    end

    describe 'PATCH/PUT /api/tickets/:id'

    describe 'PATCH/PUT /api/tickets/:id/status'

    describe 'DELETE /api/tickets/:id' do
        it 'should ticket status to cancelled' do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)
            
            delete "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(200)
            
            @ticket = Ticket.find(@ticket.id)
            expect(@ticket.status).to eq('Отменена')
        end
    end
end
