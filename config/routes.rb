Rails.application.routes.draw do
  root 'users#index'

  # gets template from app/views/templates folder
  get 'template/:subdir/:name' => 'templates#show'
  get 'template/:name' => 'templates#show'

  # gets users' login and registration page
  get 'login' => 'users#login'

  # new password page
  get 'reset/:key' => 'password_reset#reset'

  # account email confirmation
  get '/users/confirm/:key' => 'users#confirm'

  # modes for company user
  put '/users/mode/company' => 'users#set_type_company'
  patch '/users/mode/company' => 'users#set_type_company'
  put '/users/mode/client' => 'users#set_type_client'
  patch '/users/mode/client' => 'users#set_type_client'

  ### API ###

  scope :api do
    patch 'tickets/:id/status' => 'tickets#update_status', defaults: { :format => 'json' }
    put 'tickets/:id/status' => 'tickets#update_status', defaults: { :format => 'json' }
    delete 'tickets/:id' => 'tickets#cancel_ticket', defaults: { :format => 'json' }

    resources :tickets, only: [
        :index, 
        :show, 
        :create, 
        :update
      ], defaults: { :format => 'json' }

    resources :reports, only: [
        :index, 
        :show, 
        :create, 
        :update, :destroy
      ], defaults: { :format => 'json' }

    resources :rooms, only: [
        :index, 
        :show, 
        :create, 
        :update, :destroy
      ], defaults: { :format => 'json' }


    get 'estates/search/:query' => 'estates#search', defaults: { :format => 'json' }
    get 'estates/:id/file/:filetype' => 'estates#get_file', defaults: { :format => 'json' }
    post 'estates/:id/file/:filetype' => 'estates#load_file', defaults: { :format => 'json' }
    delete 'estates/:id/file/:filetype' => 'estates#destroy_file', defaults: { :format => 'json' }

    resources :estates, only: [:index, :show, :create, :update, :destroy],
      defaults: { :format => 'json' } do
        resources :supports,
          only: [:index, :show, :create, :update, :destroy],
          defaults: { :format => 'json' }
    end

    scope :users do
      get 'me' => 'users#me', format: 'json'
      post 'profile' => 'users#update', format: 'json'
      post 'upload_userpic' => 'users#upload_userpic'
      delete 'delete_userpic' => 'users#delete_userpic'
      post 'change_password' => 'users#change_password'
    end

    scope :companies do
      post 'update' => 'companies#update'
      post 'update/:id' => 'companies#update'

      post 'upload_file/:type' => 'companies#upload_file'
      delete 'delete_file/:type' => 'companies#delete_file'

      get 'innkpp_file' => 'companies#innkpp_file'
      get 'ogrn_file' => 'companies#ogrn_file'
    end

    scope :employees do
      get 'list' => 'employees#list'
      get '/:id' => 'employees#show'
      post '/' => 'employees#create'
      put '/:id' => 'employees#update'
      delete '/:id' => 'employees#delete'
    end

    scope :roles do
      get 'list' => 'roles#list'
      get 'description' => 'roles#description'
      get '/:id' => 'roles#show'
      post '/' => 'roles#create'
      put '/:id' => 'roles#update'
      delete '/:id' => 'roles#delete'
    end
  end

  # Users
  post 'login' => 'users#auth'
  get 'logout' => 'users#logout'
  post 'users' => 'users#create'

  # Password reset
  post 'reset/:key' => 'password_reset#update_password'
  post 'reset' => 'password_reset#create'
end
